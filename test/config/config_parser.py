import configparser

class ConfigParser:

    def __init__(self, filename):
        """Read ini file"""
        self.config = configparser.ConfigParser()
        self.config.read(filename, encoding='utf-8')

    def get_host(self):
        """Get host"""
        return self.config.get('BA-DB','host')

    def get_db_name(self):
        """Get db name"""
        return self.config.get('BA-DB','database')

    def get_port(self):
        """Get port"""
        return self.config.get('BA-DB','port')

    def get_username(self):
        """Get username"""
        return self.config.get('BA-DB','username')

    def get_password(self):
        """Get password"""
        return self.config.get('BA-DB','password')

    def get_schema(self):
        """Get schema"""
        return self.config.get('BA-DB','schema')

if __name__ == '__main__':
    
    """Test"""
    config = ConfigParser('config.ini')
    print (config.get_host())
    
    schema = ConfigParser('schema.ini')
    print (schema.get_schema())

    
   
