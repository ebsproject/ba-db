"""This is a tool to generate sql files from .cfg files"""

import json
import sys


input_file = sys.argv[1]


config_data = None
with open(input_file) as f:
    config_data = json.load(f)

if not config_data:
    print("No data file")
    sys.exit(0)

config_metadata = config_data["Config_Metadata"]
config_id = config_metadata["config_id"]
config_code = f"config_{config_id}.cfg"
config_name = config_metadata["config_name"]
description = config_metadata["description"]
engine = config_metadata["engine"]

print(f"""--liquibase formatted sql

--changeset postgres:add_{config_id}_ac_data context:template splitStatements:false rollbackSplitStatements:false
--comment: Add analysis configuration data for {engine} engine {config_id}
""")
print(f"""
-- {config_code}
WITH analysis_config AS (
	INSERT INTO af.property(
		code, "name", "label", description, "type", data_type
	) VALUES (
		'{config_code}', '{config_name}',
		'{config_name}', 
		'{description}', 
		'catalog_item', 'character varying'
	) RETURNING id
)
INSERT INTO af.property_config(
	order_number, creation_timestamp, creator_id,
	is_void, property_id, config_property_id, is_layout_variable
) VALUES (
	1, 'now()', '1', false, 
	(SELECT id FROM af.property WHERE code = 'analysis_config'), 
	(SELECT id FROM analysis_config), false
);
""")

module = config_data["Analysis_Module"]

# adding the stat factors
for order, field in enumerate(module["fields"], 1):
    print(f"-- add {field['stat_factor']} as stat factor")
    print(
        "INSERT INTO af.property_config (\n"
	    "    order_number, creation_timestamp, creator_id, is_void,property_id,\n"
	    "    config_property_id, is_layout_variable\n"
        ") VALUES (\n"
        f"    {order}, 'now()', '1', false,\n"
	    f"    (SELECT id FROM af.property WHERE code = '{config_code}'),\n"
	    f"    (SELECT id FROM af.property WHERE code = '{field['stat_factor']}' AND data_type = 'factor'), false\n"  
        ");\n\n"
    )
    

# adding metadata

print(f"-- add {config_id} metadata\n")
print(
    "WITH analysis_config AS (\n"
	f"    SELECT id FROM af.property WHERE code = '{config_code}'\n"
    ")\n"
    "INSERT INTO af.property_meta(code,value,property_id) VALUES"
)

print(f"    ('config_version', '{config_metadata['config_version']}', (SELECT id FROM analysis_config)),")
print(f"    ('date', '{config_metadata['date']}', (SELECT id FROM analysis_config)),")
print(f"    ('author', '{config_metadata['author']}', (SELECT id FROM analysis_config)),")
print(f"    ('email', '{config_metadata['email']}', (SELECT id FROM analysis_config)),")
print(f"    ('engine', '{config_metadata['engine']}', (SELECT id FROM analysis_config)),")

for design in config_metadata["experiment_info"]["design"] or []:
    print(f"        ('design',  '{design}', (SELECT id FROM analysis_config)),")

print(f"    ('trait_level', '{config_metadata['trait_level']}', (SELECT id FROM analysis_config)),")

info_items = ""
for key, value in config_metadata["analysis_info"].items():
    info_items += f"        ('{key}', '{value}', (SELECT id FROM analysis_config)),\n"

info_items = info_items[:-2] + ";\n"
print(info_items)

for idx, asreml_opt in enumerate(module.get("asreml_options", []), 1):
    print(f"-- add asreml options to {config_id}")
    print(
        "SELECT af.add_analysis_config_property(\n"
	    f"    'asreml_opt{idx}', 'asreml_opt{idx}', 'asreml_opt{idx}',\n"
        f"    '{asreml_opt['options']}', 'asreml_options', '{config_code}');\n\n"
    )

for idx, formula_opt in enumerate(module.get("formula"), 1):
    print(f"-- add formula to {config_id}")
    print(
        "SELECT af.add_analysis_config_property(\n"
	    f"    'formula_opt{idx}', '{formula_opt['name']}', '{formula_opt['name']}',\n"
	    f"    '{formula_opt['statement']}', 'formula', '{config_code}');\n\n"
    )

for idx, residual_opt in enumerate(module.get("residual"), 1):
    print(f"-- add residual to {config_id}")
    if "spatial_id" in residual_opt:
        print(
            "SELECT af.add_analysis_config_property(\n"
            f"    'residual_opt{idx}', '{residual_opt['spatial_name']}', '{residual_opt['spatial_name']}',\n"
            f"    '{residual_opt['spatial_model']}', 'residual', '{config_code}');\n\n"
        )
    else:
        print(
            "SELECT af.add_analysis_config_property(\n"
            f"    'residual_opt{idx}', '{residual_opt['name']}', '{residual_opt['name']}',\n"
            f"    '{residual_opt['statement']}', 'residual', '{config_code}');\n\n"
        )


for _, predict_opt in enumerate(module.get("predict"), 1):
    print(f"-- add prediction to {config_id}")
    predict_code = str(predict_opt['name']).lower()
    print(
        "SELECT af.add_analysis_config_property(\n"
	    f"    '{predict_code}', '{predict_opt['name']}', '{predict_opt['name']}',\n"
	    f"    '{predict_opt['statement']}', 'prediction', '{config_code}');\n\n"
    )


