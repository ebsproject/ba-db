--liquibase formatted sql

--changeset postgres:remove_prep_error_message context:template splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2396 Remove Prep error message



DELETE FROM af.property_rule WHERE expression = '(nRep>=2)for(nTest*0.2)';



-- revert changes
--rollback INSERT INTO
--rollback     af.property_rule
--rollback         (type, expression, id, property_config_id, order_number, notification)
--rollback VALUES
--rollback     (
--rollback         'validation-design',
--rollback         '(nRep>=2)for(nTest*0.2)',
--rollback         83,
--rollback         277,
--rollback         2,
--rollback         'At least 20% of the test entries shoulde have at least 2 number of replicates.'
--rollback     )
--rollback ;