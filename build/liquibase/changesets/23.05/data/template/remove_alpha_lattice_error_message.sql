--liquibase formatted sql

--changeset postgres:remove_alpha_lattice_error_message context:template splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2397 Remove alpha lattice error message



DELETE FROM af.property_rule WHERE expression = 'value<=(1500/totalEntries)';



-- revert changes
--rollback INSERT INTO
--rollback     af.property_rule
--rollback         (type, expression, id, property_config_id, order_number, notification)
--rollback VALUES
--rollback     (
--rollback         'validation-design',
--rollback         'value<=(1500/totalEntries)',
--rollback         49,
--rollback         78,
--rollback         1,
--rollback         'Total number of experimental units should not exceed 1,500.'
--rollback     )
--rollback ;