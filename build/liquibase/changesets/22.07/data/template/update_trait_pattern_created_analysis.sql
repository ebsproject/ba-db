--liquibase formatted sql

--changeset postgres:update_trait_pattern_created_analysis context:template splitStatements:false rollbackSplitStatements:false
--comment: Update the trait_analysis_pattern_id of existing analysis with null trait_analysis_id

-- Update analysis
UPDATE af.analysis
SET trait_analysis_pattern_id = (SELECT id from af.property WHERE code='uv' and name='Univariate')
WHERE trait_analysis_pattern_id IS NULL;