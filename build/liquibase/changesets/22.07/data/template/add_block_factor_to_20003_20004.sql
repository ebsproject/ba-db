--liquibase formatted sql

--changeset postgres:add_block_factor_20003_20004 context:template splitStatements:false rollbackSplitStatements:false
--comment: Add block factor to cfg 20003 and 20004

-- add new "factors": block, colblock and rowblock

INSERT INTO af.property
(code, "name", "label", description, "type", data_type, creator_id, is_void, tenant_id,"statement")
VALUES
('block', '', '', '', 'catalog_item', 'factor', '1', false, 1, NULL),
('rowblock', '', '', '' ,'catalog_item', 'factor', '1', false, 1, NULL),
('colblock', '', '', '' ,'catalog_item', 'factor', '1', false, 1, NULL)
;

-- add property metas
INSERT INTO af.property_meta(
    code, "value", is_void, property_id
) VALUES (
    'definition', 'blk', false, 
    (SELECT id FROM af.property WHERE code = 'block' AND data_type = 'factor')
);


INSERT INTO af.property_meta(
    code, "value", is_void, property_id
) VALUES (
    'definition', 'rowblk', false, 
    (SELECT id FROM af.property WHERE code = 'rowblock' AND data_type = 'factor')
);


INSERT INTO af.property_meta(
    code, "value", is_void, property_id
) VALUES (
    'definition', 'colblk', false, 
    (SELECT id FROM af.property WHERE code = 'colblock' AND data_type = 'factor')
);



-- add block as stat factor to 20003 and 20004
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    8, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20003.cfg'),
    (SELECT id FROM af.property WHERE code = 'block' AND data_type = 'factor'), false
);

-- add block as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    8, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20004.cfg'),
    (SELECT id FROM af.property WHERE code = 'block' AND data_type = 'factor'), false
);

-- UPDATE previouse entries "
-- "config-20005"
UPDATE af.property_config
SET config_property_id = (SELECT id FROM af.property WHERE code = 'rowblock' AND data_type = 'factor')
WHERE property_id = (SELECT id FROM af.property WHERE code = 'config_20005.cfg')
AND order_number=8;

UPDATE af.property_config
SET config_property_id = (SELECT id FROM af.property WHERE code = 'colblock' AND data_type = 'factor')
WHERE property_id = (SELECT id FROM af.property WHERE code = 'config_20005.cfg')
AND order_number=9;

-- "config-20006"
UPDATE af.property_config
SET config_property_id = (SELECT id FROM af.property WHERE code = 'rowblock' AND data_type = 'factor')
WHERE property_id = (SELECT id FROM af.property WHERE code = 'config_20006.cfg')
AND order_number=8;

UPDATE af.property_config
SET config_property_id = (SELECT id FROM af.property WHERE code = 'colblock' AND data_type = 'factor')
WHERE property_id = (SELECT id FROM af.property WHERE code = 'config_20006.cfg')
AND order_number=9;

-- "config-20023"
UPDATE af.property_config
SET config_property_id = (SELECT id FROM af.property WHERE code = 'block' AND data_type = 'factor')
WHERE property_id = (SELECT id FROM af.property WHERE code = 'config_20023.cfg')
AND order_number=8;

-- "config-20026"
UPDATE af.property_config
SET config_property_id = (SELECT id FROM af.property WHERE code = 'block' AND data_type = 'factor')
WHERE property_id = (SELECT id FROM af.property WHERE code = 'config_20026.cfg')
AND order_number=8;

-- "config-20035"
UPDATE af.property_config
SET config_property_id = (SELECT id FROM af.property WHERE code = 'block' AND data_type = 'factor')
WHERE property_id = (SELECT id FROM af.property WHERE code = 'config_20035.cfg')
AND order_number=8;

-- "config-20041"
UPDATE af.property_config
SET config_property_id = (SELECT id FROM af.property WHERE code = 'block' AND data_type = 'factor')
WHERE property_id = (SELECT id FROM af.property WHERE code = 'config_20041.cfg')
AND order_number=8;


-- "config-20028"
UPDATE af.property_config
SET config_property_id = (SELECT id FROM af.property WHERE code = 'block' AND data_type = 'factor')
WHERE property_id = (SELECT id FROM af.property WHERE code = 'config_20028.cfg')
AND order_number=7;

-- "config-20029"
UPDATE af.property_config
SET config_property_id = (SELECT id FROM af.property WHERE code = 'block' AND data_type = 'factor')
WHERE property_id = (SELECT id FROM af.property WHERE code = 'config_20029.cfg')
AND order_number=7;

-- "config-20030"
UPDATE af.property_config
SET config_property_id = (SELECT id FROM af.property WHERE code = 'block' AND data_type = 'factor')
WHERE property_id = (SELECT id FROM af.property WHERE code = 'config_20030.cfg')
AND order_number=7;

-- "config-20032"
UPDATE af.property_config
SET config_property_id = (SELECT id FROM af.property WHERE code = 'block' AND data_type = 'factor')
WHERE property_id = (SELECT id FROM af.property WHERE code = 'config_20032.cfg')
AND order_number=7;








