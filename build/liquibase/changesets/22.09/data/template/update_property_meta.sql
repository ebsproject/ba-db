--liquibase formatted sql

--changeset postgres:update_property_meta.sql context:template splitStatements:false rollbackSplitStatements:false
--comment: Update property meta



UPDATE af.property_meta SET VALUE = 'randAugUnrep' WHERE property_id = (SELECT id FROM af.property WHERE code = 'randAugUnrep') and code='method';

UPDATE af.property_meta SET VALUE = 'Rscript randAugUnrep.R --entryList ''randAugUnrep_SD_0001.lst'' --nTrial 3 --pCheck 10 --nFieldCol 20' WHERE property_id = (SELECT id FROM af.property WHERE code = 'randAugUnrep') AND code='syntax';