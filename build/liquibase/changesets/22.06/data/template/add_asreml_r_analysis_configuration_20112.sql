--liquibase formatted sql

--changeset postgres:add_20112_ac_data context:template splitStatements:false rollbackSplitStatements:false
--comment: Add analysis configuration data for ASREML-R engine 20112


-- config_20112.cfg
WITH analysis_config AS (
	INSERT INTO af.property(
		code, "name", "label", description, "type", data_type
	) VALUES (
		'config_20112.cfg', 'Multi-Exp 2-stage analysis - 2nd Stage - random model',
		'Multi-Exp 2-stage analysis - 2nd Stage - random model', 
		'Executes the second stage of a two stage analysis, genotype as random, corgh structure', 
		'catalog_item', 'character varying'
	) RETURNING id
)
INSERT INTO af.property_config(
	order_number, creation_timestamp, creator_id,
	is_void, property_id, config_property_id, is_layout_variable
) VALUES (
	1, 'now()', '1', false, 
	(SELECT id FROM af.property WHERE code = 'analysis_config'), 
	(SELECT id FROM analysis_config), false
);

-- add loc as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    1, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20112.cfg'),
    (SELECT id FROM af.property WHERE code = 'loc' AND data_type = 'factor'), false
);


-- add expt as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    2, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20112.cfg'),
    (SELECT id FROM af.property WHERE code = 'expt' AND data_type = 'factor'), false
);


-- add ge as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    3, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20112.cfg'),
    (SELECT id FROM af.property WHERE code = 'ge' AND data_type = 'factor'), false
);


-- add 20112 metadata

WITH analysis_config AS (
    SELECT id FROM af.property WHERE code = 'config_20112.cfg'
)
INSERT INTO af.property_meta(code,value,property_id) VALUES
    ('config_version', '1.0.0', (SELECT id FROM analysis_config)),
    ('date', '02-June-2022', (SELECT id FROM analysis_config)),
    ('author', 'Alaine Gulles', (SELECT id FROM analysis_config)),
    ('email', 'a.gulles@irri.org', (SELECT id FROM analysis_config)),
    ('engine', 'ASREML-R', (SELECT id FROM analysis_config)),
    ('trait_level', 'plot', (SELECT id FROM analysis_config)),
        ('analysis_objective', 'prediction', (SELECT id FROM analysis_config)),
        ('exp_analysis_pattern', 'multi', (SELECT id FROM analysis_config)),
        ('loc_analysis_pattern', 'single', (SELECT id FROM analysis_config)),
        ('year_analysis_pattern', 'single', (SELECT id FROM analysis_config)),
        ('trait_pattern', 'univariate', (SELECT id FROM analysis_config)),
        ('genotype_effect', 'random', (SELECT id FROM analysis_config)),
        ('step', '2', (SELECT id FROM analysis_config)),
        ('field_coord', 'without', (SELECT id FROM analysis_config));

-- add asreml options to 20112
SELECT af.add_analysis_config_property(
    'asreml_opt1', 'asreml_opt1', 'asreml_opt1',
    'workspace = 128e06, method = ''em'', weights = weight', 'asreml_options', 'config_20112.cfg');


-- add formula to 20112
SELECT af.add_analysis_config_property(
    'formula_opt1', 'Analysis with genotype as random, corgh structure', 'Analysis with genotype as random, corgh structure',
    'fixed = predicted_value ~ loc, random = ~ ge + corgh(loc):ge', 'formula', 'config_20112.cfg');


-- add residual to 20112
SELECT af.add_analysis_config_property(
    'residual_opt1', 'Univariate homogeneous variance model', 'Univariate homogeneous variance model',
    '~ ~dsum(~id(units) | loc)', 'residual', 'config_20112.cfg');


-- add prediction to 20112
SELECT af.add_analysis_config_property(
    'g', 'G', 'G',
    'ge', 'prediction', 'config_20112.cfg');


-- add prediction to 20112
SELECT af.add_analysis_config_property(
    'gxe', 'GxE', 'GxE',
    'corgh(loc):ge', 'prediction', 'config_20112.cfg');


