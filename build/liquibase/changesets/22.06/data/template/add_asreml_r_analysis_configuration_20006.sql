--liquibase formatted sql

--changeset postgres:add_20006_ac_data context:template splitStatements:false rollbackSplitStatements:false
--comment: Add analysis configuration data for ASREML-R engine 20006


-- config_20006.cfg
WITH analysis_config AS (
	INSERT INTO af.property(
		code, "name", "label", description, "type", data_type
	) VALUES (
		'config_20006.cfg', 'Row-Column multi-location - ASReml-R - One stage - entry as random',
		'Row-Column multi-location - ASReml-R - One stage - entry as random', 
		'Row-Column multi-location and univariate trial with options for spatial adjustment', 
		'catalog_item', 'character varying'
	) RETURNING id
)
INSERT INTO af.property_config(
	order_number, creation_timestamp, creator_id,
	is_void, property_id, config_property_id, is_layout_variable
) VALUES (
	1, 'now()', '1', false, 
	(SELECT id FROM af.property WHERE code = 'analysis_config'), 
	(SELECT id FROM analysis_config), false
);

-- add loc as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    1, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20006.cfg'),
    (SELECT id FROM af.property WHERE code = 'loc' AND data_type = 'factor'), false
);


-- add expt as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    2, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20006.cfg'),
    (SELECT id FROM af.property WHERE code = 'expt' AND data_type = 'factor'), false
);


-- add entry as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    3, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20006.cfg'),
    (SELECT id FROM af.property WHERE code = 'entry' AND data_type = 'factor'), false
);


-- add plot as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    4, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20006.cfg'),
    (SELECT id FROM af.property WHERE code = 'plot' AND data_type = 'factor'), false
);


-- add col as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    5, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20006.cfg'),
    (SELECT id FROM af.property WHERE code = 'col' AND data_type = 'factor'), false
);


-- add row as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    6, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20006.cfg'),
    (SELECT id FROM af.property WHERE code = 'row' AND data_type = 'factor'), false
);


-- add rep as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    7, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20006.cfg'),
    (SELECT id FROM af.property WHERE code = 'rep' AND data_type = 'factor'), false
);


-- add rowblock as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    8, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20006.cfg'),
    (SELECT id FROM af.property WHERE code = 'rowblock' AND data_type = 'factor'), false
);


-- add colblock as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    9, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20006.cfg'),
    (SELECT id FROM af.property WHERE code = 'colblock' AND data_type = 'factor'), false
);


-- add 20006 metadata

WITH analysis_config AS (
    SELECT id FROM af.property WHERE code = 'config_20006.cfg'
)
INSERT INTO af.property_meta(code,value,property_id) VALUES
    ('config_version', '1.0.0', (SELECT id FROM analysis_config)),
    ('date', '29-Mar-2022', (SELECT id FROM analysis_config)),
    ('author', 'Alaine Gulles | Pedro Barbosa', (SELECT id FROM analysis_config)),
    ('email', 'a.gulles@irri.org | p.medeiros@cgiar.org', (SELECT id FROM analysis_config)),
    ('engine', 'ASREML-R', (SELECT id FROM analysis_config)),
        ('design',  'Row-Column', (SELECT id FROM analysis_config)),
    ('trait_level', 'plot', (SELECT id FROM analysis_config)),
        ('analysis_objective', 'prediction', (SELECT id FROM analysis_config)),
        ('exp_analysis_pattern', 'single', (SELECT id FROM analysis_config)),
        ('loc_analysis_pattern', 'multi', (SELECT id FROM analysis_config)),
        ('year_analysis_pattern', 'single', (SELECT id FROM analysis_config)),
        ('trait_pattern', 'univariate', (SELECT id FROM analysis_config));

-- add formula to 20006
SELECT af.add_analysis_config_property(
    'formula_opt1', 'Row-Column SEML Univariate. Replicate and Location as fixed and Genotype as random effect', 'Row-Column SEML Univariate. Replicate and Location as fixed and Genotype as random effect',
    'fixed ={trait_name} ~ loc + loc:rep, random =~ entry + rep:rowblock + rep:colblock + loc:entry', 'formula', 'config_20006.cfg');


-- add residual to 20006
SELECT af.add_analysis_config_property(
    'residual_opt1', 'Univariate homogeneous variance modelt', 'Univariate homogeneous variance modelt',
    '~dsum(~id(units)|loc)', 'residual', 'config_20006.cfg');


-- add residual to 20006
SELECT af.add_analysis_config_property(
    'residual_opt2', 'Autoregressive order 1 spatial structure (AR1row x AR1col), by location', 'Autoregressive order 1 spatial structure (AR1row x AR1col), by location',
    '~dsum(~ar1(row):ar1(col)|loc)', 'residual', 'config_20006.cfg');


-- add residual to 20006
SELECT af.add_analysis_config_property(
    'residual_opt3', 'Autoregressive order 1 spatial structure for rows (AR1row x IDcol), by location', 'Autoregressive order 1 spatial structure for rows (AR1row x IDcol), by location',
    '~dsum(~ar1(row):id(col)|loc)', 'residual', 'config_20006.cfg');


-- add residual to 20006
SELECT af.add_analysis_config_property(
    'residual_opt4', 'Autoregressive order 1 spatial structure for columns (IDrow x AR1col), by location', 'Autoregressive order 1 spatial structure for columns (IDrow x AR1col), by location',
    '~dsum(~id(row):ar1(col)|loc)', 'residual', 'config_20006.cfg');


-- add prediction to 20006
SELECT af.add_analysis_config_property(
    'g', 'G', 'G',
    'entry', 'prediction', 'config_20006.cfg');


-- add prediction to 20006
SELECT af.add_analysis_config_property(
    'gxe', 'GxE', 'GxE',
    'loc:entry', 'prediction', 'config_20006.cfg');


