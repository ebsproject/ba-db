--liquibase formatted sql

--changeset postgres:add_20023_ac_data context:template splitStatements:false rollbackSplitStatements:false
--comment: Add analysis configuration data for ASREML-R engine 20023


-- config_20023.cfg
WITH analysis_config AS (
	INSERT INTO af.property(
		code, "name", "label", description, "type", data_type
	) VALUES (
		'config_20023.cfg', 'Multi-Exp 2-stage analysis - 1st Stage - Alpha - fix model - no spatial',
		'Multi-Exp 2-stage analysis - 1st Stage - Alpha - fix model - no spatial', 
		'Executes the first stage of a two stage analysis for Alpha Lattice experiments, genotype as fixed, no spatial adjustment', 
		'catalog_item', 'character varying'
	) RETURNING id
)
INSERT INTO af.property_config(
	order_number, creation_timestamp, creator_id,
	is_void, property_id, config_property_id, is_layout_variable
) VALUES (
	1, 'now()', '1', false, 
	(SELECT id FROM af.property WHERE code = 'analysis_config'), 
	(SELECT id FROM analysis_config), false
);

-- add loc as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    1, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20023.cfg'),
    (SELECT id FROM af.property WHERE code = 'loc' AND data_type = 'factor'), false
);


-- add expt as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    2, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20023.cfg'),
    (SELECT id FROM af.property WHERE code = 'expt' AND data_type = 'factor'), false
);


-- add ge as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    3, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20023.cfg'),
    (SELECT id FROM af.property WHERE code = 'ge' AND data_type = 'factor'), false
);


-- add plot as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    4, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20023.cfg'),
    (SELECT id FROM af.property WHERE code = 'plot' AND data_type = 'factor'), false
);


-- add col as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    5, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20023.cfg'),
    (SELECT id FROM af.property WHERE code = 'col' AND data_type = 'factor'), false
);


-- add row as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    6, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20023.cfg'),
    (SELECT id FROM af.property WHERE code = 'row' AND data_type = 'factor'), false
);


-- add rep as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    7, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20023.cfg'),
    (SELECT id FROM af.property WHERE code = 'rep' AND data_type = 'factor'), false
);


-- add block as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    8, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20023.cfg'),
    (SELECT id FROM af.property WHERE code = 'block' AND data_type = 'factor'), false
);


-- add 20023 metadata

WITH analysis_config AS (
    SELECT id FROM af.property WHERE code = 'config_20023.cfg'
)
INSERT INTO af.property_meta(code,value,property_id) VALUES
    ('config_version', '1.0.0', (SELECT id FROM analysis_config)),
    ('date', '16-May-2022', (SELECT id FROM analysis_config)),
    ('author', 'Alaine Gulles', (SELECT id FROM analysis_config)),
    ('email', 'a.gulles@irri.org', (SELECT id FROM analysis_config)),
    ('engine', 'ASREML-R', (SELECT id FROM analysis_config)),
        ('design',  'Alpha-Lattice', (SELECT id FROM analysis_config)),
    ('trait_level', 'plot', (SELECT id FROM analysis_config)),
        ('analysis_objective', 'prediction', (SELECT id FROM analysis_config)),
        ('exp_analysis_pattern', 'multi', (SELECT id FROM analysis_config)),
        ('loc_analysis_pattern', 'single', (SELECT id FROM analysis_config)),
        ('year_analysis_pattern', 'single', (SELECT id FROM analysis_config)),
        ('trait_pattern', 'univariate', (SELECT id FROM analysis_config)),
        ('genotype_effect', 'fixed', (SELECT id FROM analysis_config)),
        ('step', '1', (SELECT id FROM analysis_config)),
        ('field_coord', 'without', (SELECT id FROM analysis_config));

-- add asreml options to 20023
SELECT af.add_analysis_config_property(
    'asreml_opt1', 'asreml_opt1', 'asreml_opt1',
    'na.action = na.method(y = ''include'', x = ''include''), workspace = 128e06', 'asreml_options', 'config_20023.cfg');


-- add formula to 20023
SELECT af.add_analysis_config_property(
    'formula_opt1', 'Analysis with genotype as fixed - Alpha', 'Analysis with genotype as fixed - Alpha',
    'fixed = {trait_name} ~ rep + ge, random = ~ rep:block', 'formula', 'config_20023.cfg');


-- add residual to 20023
SELECT af.add_analysis_config_property(
    'residual_opt1', 'Univariate homogeneous variance model', 'Univariate homogeneous variance model',
    '~id(units)', 'residual', 'config_20023.cfg');


-- add prediction to 20023
SELECT af.add_analysis_config_property(
    'g', 'G', 'G',
    'ge', 'prediction', 'config_20023.cfg');


