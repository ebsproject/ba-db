--liquibase formatted sql

--changeset postgres:add_metadata_me_rcbd context:template splitStatements:false rollbackSplitStatements:false
--comment: Add analysis configuration data for ASREML-R engine 20002

-- add config_20022 metadata
WITH analysis_config AS (
	SELECT id FROM af.property WHERE code = 'config_20022.cfg'
)
INSERT INTO af.property_meta(code,value,property_id) VALUES
    	('genotype_effect', 'fixed', (SELECT id FROM analysis_config)),
    	('step', '1', (SELECT id FROM analysis_config)),
    	('field_coord', 'without', (SELECT id FROM analysis_config));


-- add config_20025 metadata
WITH analysis_config AS (
	SELECT id FROM af.property WHERE code = 'config_20025.cfg'
)
INSERT INTO af.property_meta(code,value,property_id) VALUES
    	('genotype_effect', 'fixed', (SELECT id FROM analysis_config)),
    	('step', '1', (SELECT id FROM analysis_config)),
    	('field_coord', 'with', (SELECT id FROM analysis_config));


    -- add config_20034 metadata
WITH analysis_config AS (
	SELECT id FROM af.property WHERE code = 'config_20034.cfg'
)
INSERT INTO af.property_meta(code,value,property_id) VALUES
    	('genotype_effect', 'random', (SELECT id FROM analysis_config)),
    	('step', '1', (SELECT id FROM analysis_config)),
    	('field_coord', 'without', (SELECT id FROM analysis_config));


-- add config_20040 metadata
WITH analysis_config AS (
	SELECT id FROM af.property WHERE code = 'config_20040.cfg'
)
INSERT INTO af.property_meta(code,value,property_id) VALUES
    	('genotype_effect', 'random', (SELECT id FROM analysis_config)),
    	('step', '1', (SELECT id FROM analysis_config)),
    	('field_coord', 'with', (SELECT id FROM analysis_config));