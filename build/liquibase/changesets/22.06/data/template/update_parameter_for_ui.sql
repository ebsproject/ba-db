--liquibase formatted sql

--changeset postgres:update_parameter_for_ui context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1291 Update parameter in BADB to be reflected un the UI



update af.property_ui 
set is_catalogue = true 
where id in (select property_ui_id from af.property_config where config_property_id in (select id from af.property where code='serpentine'));

update af.property 
set data_type = 'character varying' 
where code='serpentine';

update af.property_rule 
set "group"=0;

update af.property_config 
set is_required = FALSE 
where property_id = (select id from af.property where code = 'randIBD_OFT') and config_property_id = (select id from af.property where code = 'outputPath');

update af.property_config 
set is_required = FALSE 
where property_id = (select id from af.property where code = 'randRCBDirri_OFT') and config_property_id = (select id from af.property where code = 'outputPath');
