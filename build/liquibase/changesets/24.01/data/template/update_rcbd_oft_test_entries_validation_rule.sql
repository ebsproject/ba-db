--liquibase formatted sql

--changeset postgres:update_rcbd_oft_test_entries_validation_rule context:template splitStatements:false rollbackSplitStatements:false
--comment: BA-2432 Update RCBD | OFT test entries validation rule

UPDATE
    af.property_rule
SET
    expression = 'nEntries>=3'
WHERE
    expression = 'nEntries>3'
    AND type = 'validation-design'
    AND notification = 'Total number of test entries should be at least 3.'
    AND action = 'error'
;

-- revert changes
--rollback UPDATE
--rollback     af.property_rule
--rollback SET
--rollback     expression = 'nEntries>3'
--rollback WHERE
--rollback     expression = 'nEntries>=3'
--rollback     AND type = 'validation-design'
--rollback     AND notification = 'Total number of test entries should be at least 3.'
--rollback     AND action = 'error'
--rollback ;
