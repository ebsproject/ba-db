--liquibase formatted sql

--changeset postgres:update_property_description.sql context:template splitStatements:false rollbackSplitStatements:false
--comment: Update property description



UPDATE af.property 
SET description = 'Randomize the experiment as an Alpha-Lattice (0,1). User can set the number of replicates and block size. Recommended when there are concerns regarding field heterogeneity and mid-to-large number of entries. Not available if the number of entries is a prime number.' 
WHERE code = 'randALPHALATTICEcimmyt';

UPDATE af.property 
SET description = 'Randomize the experiment as an Alpha-Lattice (0,1). User can set the number of replicates and block size. Recommended when there are concerns regarding field  heterogeneity and mid-to-large number of entries. User can set to use the same randomization to all occurrences, if you will. Not available if the number of entries is a prime number.' 
WHERE code = 'randALPHALATTICEcimmytWHEAT';

UPDATE af.property 
SET description = 'Randomize the experiment as an Alpha-Lattice (0,1). User can set the number of replicates and block size. Recommended when there are concerns regarding field heterogeneity and mid-to-large number of entries. Not available if the number of entries is a prime number.' 
where code = 'randALPHALATTICEirri';

UPDATE af.property 
SET description = 'Randomize the check entries in a RCBD. User can set the number of blocks. The set of test entries is randomized and divided by the number of blocks creating subsets, each subset is randomly assigned to a block. Recommended for early generation trials, where there isn''t enough amount of seeds of test entries to create complete replicated blocks.' 
where code = 'randAUGMENTEDRCBDirri';

UPDATE af.property 
SET description = 'Checks are classified into two groups, spatial and replicated. The spatial checks are assigned to the field in a diagonal pattern, user can set the percentage of total plots designated to the diagonals. The replicated checks are randomly distributed to the field, user can set the number plots of each. One plot of each test entry is randomly assigned to the field. Recommended for early-generation trials when there is a large number of genotypes under evaluation and there isn''t enough seeds of most of them to plant replicates.' 
WHERE code = 'randAugUnrep';

UPDATE af.property 
SET description = 'Exclusively for experiments in On-Farm trial stage. It creates an Incomplete Block Design and each block is assigned to an occurrence. You can define the number of occurrences. There is a restriction in the number of blocks, since it should be divisible by the number of entries and also a restriction to garante a balanced design.' 
WHERE code = 'randIBD_OFT';

UPDATE af.property 
SET description = 'Randomize the experiment as a Partially Replicated Design. You can set the number of occurrences and the number of times each entry will be replicated. It requires that at least 20% of the test entries are replicated.' 
WHERE code = 'randPREPirri';

UPDATE af.property 
SET description = 'Randomize the experiment as a Randomized Complete Block Design, where all genotypes are present in each block. User can set the number of blocks. Recommended to advanced stages when a small number of genotypes are under evaluation and there are no big concerns regarding field heterogeneity.' 
WHERE code = 'randRCBDcimmyt';

UPDATE af.property 
SET description = 'Randomize the experiment as a Randomized Complete Block Design, where all genotypes are present in each block. User can set the number of blocks. Recommended to advanced stages when a small number of genotypes are under evaluation and there are no big concerns regarding field heterogeneity.' 
WHERE code = 'randRCBDirri';

UPDATE af.property 
SET description = 'Exclusively for experiments in On-Farm trial stage. It creates a RCBD where each block is assigned to an occurrence. User can define the number of occurrences.' 
WHERE code = 'randRCBDirri_OFT';

UPDATE af.property 
SET description = 'Randomize the experiment creating complete replicate and, within the replicates, randomize the entries in order to create blocks considering rows and columns. User can set the number of blocks and must define the field dimensions of each replicate. Recommended to mid-advanced stages and there some concerns regarding field heterogeneity.' 
WHERE code = 'randROWCOLUMNirri';


--rollback UPDATE af.property 
--rollback SET description = 'Randomize the experiment as an Alpha-Lattice Design' 
--rollback WHERE code = 'randALPHALATTICEcimmyt';
--rollback 
--rollback UPDATE af.property 
--rollback SET description = 'Randomize the experiment as an Alpha-Lattice Design for IWIN experiments' 
--rollback WHERE code = 'randALPHALATTICEcimmytWHEAT';
--rollback 
--rollback UPDATE af.property 
--rollback SET description = 'Randomize the experiment as an Alpha-Lattice Design' 
--rollback where code = 'randALPHALATTICEirri';
--rollback 
--rollback UPDATE af.property 
--rollback SET description = 'Randomize the experiment as an Augmented Randomized Complete Block Design' 
--rollback where code = 'randAUGMENTEDRCBDirri';
--rollback 
--rollback UPDATE af.property 
--rollback SET description = 'Randomize the experiment as a Partially Replicated Design with check entries assigned in a Diagonal pattern in the field' 
--rollback WHERE code = 'randAugUnrep';
--rollback 
--rollback UPDATE af.property 
--rollback SET description = 'Randomize the experiment as an Incomplete Block Design for On-Farm Trial experiments' 
--rollback WHERE code = 'randIBD_OFT';
--rollback 
--rollback UPDATE af.property 
--rollback SET description = 'Randomize the experiment as a Partially Replicated Design' 
--rollback WHERE code = 'randPREPirri';
--rollback 
--rollback UPDATE af.property 
--rollback SET description = 'Randomize the experiment as a Complete Block Design' 
--rollback WHERE code = 'randRCBDcimmyt';
--rollback 
--rollback UPDATE af.property 
--rollback SET description = 'Randomize the experiment as a Complete Block Design' 
--rollback WHERE code = 'randRCBDirri';
--rollback 
--rollback UPDATE af.property 
--rollback SET description = 'Randomize the experiment as aRandomized Complete Block Design for On-Farm Trial experiments' 
--rollback WHERE code = 'randRCBDirri_OFT';
--rollback 
--rollback UPDATE af.property 
--rollback SET description = 'Randomize the experiment as a Row-Column Design' 
--rollback WHERE code = 'randROWCOLUMNirri';


